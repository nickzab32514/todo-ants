import React, { useState, useEffect } from 'react';
import { Descriptions, List, Typography, Button, Select } from 'antd';

const { Option } = Select;

const ToDoPage = (props) => {

    const [user, setUser] = useState([]);
    const [toDoList, setToDoList] = useState([]);
    const [selectedTypeTodo, setSelectedTypeTodo] = useState('All');

    const fetchUserData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/users/' + userID)
            .then(respose => respose.json())
            .then(data => {
                setUser(data);
            })
    }

    const fetchToDoData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userID)
            .then(respose => respose.json())
            .then(data => {
                setToDoList(data);
            })
    }

    useEffect(() => {
        fetchUserData();
        fetchToDoData();
    }, []);
    // console.log(user)

    // const checkTypeTodo = toDoList => {
    //     let typetoDoList = [];
    //     toDoList.map((toDoList) => {
    //         if (!typetoDoList.includes(toDoList.completed)) {
    //             typetoDoList.push(toDoList.completed)
    //         }
    //     })
    //     setSelectedTypeTodo(typetoDoList)
    // }

    const changeCompleteType = (index) => {
        let newToDoList = { ...toDoList }
        newToDoList[index].completed = true
        setUser(newToDoList)
        fetchUserData();
    }

    const filterTodoList = (value) => {
        setSelectedTypeTodo(value);
        console.log(selectedTypeTodo)
    }


    return (
        < div >
            <div>
                toDoPage:{props.match.params.user_id}
            </div>

            <Descriptions title={"Infomation Of " + user.name}>
                <Descriptions.Item label="UserName">{user.name}</Descriptions.Item>
                <Descriptions.Item label="Telephone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Live">Hangzhou, Zhejiang</Descriptions.Item>
                <Descriptions.Item label="Remark">empty</Descriptions.Item>
                <Descriptions.Item label="Address">{user.email}</Descriptions.Item>
            </Descriptions>
            <Select defaultValue="All" style={{ width: 120 }} onChange={filterTodoList}>
                <Option value="All">All</Option>
                <Option value="true">Done</Option>
                <Option value="false">Doing</Option>
            </Select>
            <List
                header={<div class="row">
                    <th>To DoList</th>
                </div>}
                bordered
                dataSource={
                    selectedTypeTodo == 'All' ?
                        toDoList
                        :
                        toDoList.filter((list) => list.completed.toString() === selectedTypeTodo)
                }
                change={changeCompleteType}
                renderItem={(item, index) => (
                    <List.Item>
                        <div class="col-1">
                            {item.completed ?
                                <Typography.Text delete>Done</Typography.Text>
                                :
                                <Typography.Text mark >Doing</Typography.Text>
                            }
                        </div>
                        <div class="col-5">
                            {item.title}
                        </div>
                        {item.completed ?
                            <div></div>

                            :
                            <div class="col-5">
                                <Button type="basic" onClick={() => {
                                    let toDoListTmp = [...toDoList]//copy object in Array

                                    toDoListTmp[index].completed = true;

                                    setToDoList(toDoListTmp);
                                }
                                }>Done</Button>
                            </div>
                        }
                    </List.Item>
                )}
            />
        </div >
        // <div>sdad</div>
    )
}
export default ToDoPage;
