import React from 'react';
import { List, Avatar, Skeleton, Input } from 'antd';

import reqwest from 'reqwest';

const { Search } = Input;
const count = 3;
const fakeDataUrl = `https://randomuser.me/api/?results=${count}&inc=name,gender,email,nat&noinfo`;

class InfoUserPage extends React.Component {
    state = {
        data: [],
        list: [],
        users: []
    };

    fetchUserData = () => {
        fetch('https://jsonplaceholder.typicode.com/users')
            .then(respose => respose.json())
            .then(data => {
                this.setState({ users: data });
            })
    }

    componentDidMount() {

        this.fetchUserData()
    }


    render() {
        const { users } = this.state;


        return (
            <div>
                <Search
                    placeholder="input search user"
                    onSearch={value => console.log(value)}
                    style={{ width: 200 }}
                // onChange={searchToDoList}
                />
                <List
                    className="demo-loadmore-list"
                    itemLayout="horizontal"
                    dataSource={users}
                    renderItem={item => (
                        <List.Item
                            actions={[
                                <a href={'/users/' + item.id + '/todo'}>toDo</a>
                              , <a href={'/users/' + item.id + '/album'}>Album</a>]}  

                        >
                            <Skeleton avatar title={false} loading={item.loading} active>
                                <List.Item.Meta
                                    avatar={
                                        <Avatar src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png" />
                                    }
                                    title={<a href="https://ant.design">{item.name}</a>}
                                    description={<div class="row">
                                        <div>Email:{item.email}</div>
                                        <div>Phone{item.phone}</div>
                                    </div>}

                                />
                            </Skeleton>

                        </List.Item>
                    )}
                />
            </div>
        );
    }
}


// const InfoUserPage = (props) => {
//     console.log(props)
//     return (
//         <div>
//             <Search
//                 placeholder="input search text"
//                 onSearch={value => console.log(value)}
//                 style={{ width: 200 }}
//             />
//             <h1>InfoUserPage:{props.match.params.name}</h1>
//         </div>
//     )
// }
export default InfoUserPage;
